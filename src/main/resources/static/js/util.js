$ = layui.$;
// 获取index传参rid args为传递过来的所有参数，类型为OBJDECT，调用为args();
let args = function (params) {
    var a = {};
    params = params || location.search;
    if (!params) return {};
    params = decodeURI(params);
    params.replace(/(?:^\?|&)([^=&]+)(?:\=)([^=&]+)(?=&|$)/g, function (m, k, v) {
        a[k] = v;
    });
    return a;
};

/**
 * 2018开始的整型值转换为日期字符串 yyyy-MM-dd
 * @param params
 * @returns {string}
 */
let get20180101Date = function (params) {
    return addDate("2018-01-01", params);
};

let addDate = function (date, days) {
    if (days === undefined || days === '') {
        days = 1;
    }
    date = new Date(date);
    date.setDate(date.getDate() + days);
    let month = date.getMonth() + 1;
    let day = date.getDate();
    let mm = "'" + month + "'";
    let dd = "'" + day + "'";

    //单位数前面加0
    if (mm.length === 3) {
        month = "0" + month;
    }
    if (dd.length === 3) {
        day = "0" + day;
    }

    return date.getFullYear() + "-" + month + "-" + day;
};

let getDatetime = function () {
    let d = new Date();
    let year = d.getFullYear();
    let month = change(d.getMonth() + 1);
    let day = change(d.getDate());
    let hour = change(d.getHours());
    let minute = change(d.getMinutes());
    let second = change(d.getSeconds());

    return year + '-' + month + '-' + day + ' '
        + hour + ':' + minute + ':' + second;
};

let change = function (t) {
    if (t < 10) {
        return "0" + t;
    } else {
        return t;
    }
};

let getyymm = function () {
    //创建日期对象
    let date = new Date;
    //获取年份
    let yy = date.getFullYear();
    //获取月份
    let mm = date.getMonth() + 1;
    //如果月份小于10 前面加0
    mm = (mm < 10 ? "0" + mm : mm);
    //返回日期
    return (yy.toString() + '-' + +mm.toString());
};

let getyymmdd = function (param) {
    let d = new Date(param);
    let year = d.getFullYear();
    let month = change(d.getMonth() + 1);
    let day = change(d.getDate());
    return year + '-' + month + '-' + day;
};

/**
 * 触发select点击事件
 *
 * @param selectName select标签name属性
 * @param optionValue option选项的value值
 */
let clickSelect = function (selectName, optionValue) {
    // $(`select[name='${selectName}']`).siblings('.layui-form-select').find(`dd[lay-value='${optionValue}']`).eq(0).click();
    let option = $(`select[name='${selectName}']`).siblings('.layui-form-select').find(`dd[lay-value='${optionValue}']`)[0];
    if (option === undefined) {
        setTimeout(function () {
            clickSelect(selectName, optionValue)
        }, 100);
    } else {
        $(option).click();
    }
};

/**
 * 将复选框多选情况下,form表单的field值 name[value1] name[value2] 合并为name:'value1,value2'
 *
 * @param fields form表单的fields属性
 * @param checkboxName 复选框name
 */
let mergeCheckBoxValue = function (fields, checkboxName) {
    let ids = [];
    for (let i in fields) {
        if (i.startsWith(checkboxName)) {
            ids.push(fields[i]);
            delete fields[i];
        }
    }
    fields[checkboxName] = ids.join(',');
};

/**
 * 拆分checkbox的值，从逗号分隔切分为各自一个单独的key
 *
 * @param fields form表单的fields属性
 * @param checkboxName 复选框name
 */
let breakCheckBoxValue = function (fields, checkboxName) {
    for (let i in fields[checkboxName]) {
        fields[`${checkboxName}[${fields[checkboxName][i]}]`] = 'on'
    }
    delete fields[checkboxName];
};

// 名称前缀相同的checkbox，全选及反选.status 1-选中 0-非选中
let changeCheckBoxStatus = function (checkboxName, status) {
    let fields = $(":checkbox");
    for (let i in fields) {
        let checkbox = fields[i];
        if (checkbox.name != null && checkbox.name !== '') {
            if (checkbox.name.startsWith(checkboxName)) {
                if (1 === status) {
                    $(checkbox).prop("checked", true);
                } else {
                    $(checkbox).prop("checked", false);
                }
            }
        }
    }
};

/**
 * 操作父页面打开一个新的选项卡
 *
 * @param title 选项卡显示的标题名
 * @param url 打开的地址 可以传递get参数
 * @param id 唯一ID 不可重复 重复则无法打开新的选项卡
 */
let openNewTab = function (title, url, id) {
    parent.layout.addMenu(title, url, id, false);
};

/**
 * 去除表单JSON中的空JSON
 *
 * @param fields
 * @returns {*|{}}
 */
let removeNullFields = function (fields) {
    let params = {};
    for (let i in fields) {
        if (fields[i] !== '-1' && fields[i] !== '') {
            params[i] = fields[i];
        }
    }
    params = params.length === 0 ? null : params;
    return params;
};

/**
 * 获取相对今天的日期，>0则向后推，<0则向前推
 * @param day
 * @returns {string}
 */
let getDay = function (day) {
    let today = new Date();
    let targetday_milliseconds = today.getTime() + 1000 * 60 * 60 * 24 * day;
    today.setTime(targetday_milliseconds); //注意，这行是关键代码
    let tYear = today.getFullYear();
    let tMonth = today.getMonth();
    let tDate = today.getDate();
    tMonth = doHandleMonth(tMonth + 1);
    tDate = doHandleMonth(tDate);
    return tYear + "-" + tMonth + "-" + tDate;
}

/**
 * 处理1-9月前缀0的问题
 * @param month
 * @returns {string}
 */
let doHandleMonth = function (month) {
    let m = month;
    if (month.toString().length === 1) {
        m = "0" + month;
    }
    return m;
}

/**
 * 判断字符是否为空的方法
 * @param obj
 * @returns {boolean}
 */
let isEmpty = function (obj) {
    if (typeof obj === undefined || obj === null || obj === "" || obj.length === 0) {
        return true;
    } else {
        return false;
    }
}
//判断内容是否为空
let isNull = function (str) {
    if (str == null || str == "" || str == '' || str == "null" || str == "undefined") {
        return true;
    } else {
        return false;
    }
}