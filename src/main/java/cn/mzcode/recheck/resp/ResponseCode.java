package cn.mzcode.recheck.resp;

/**
 * 响应数据状态码
 */
public enum ResponseCode {
    /**
     * 成功
     */
    SUCCESS(0, "成功"),
    /**
     * 通用失败
     */
    ERROR(1, "失败"),
    /**
     * 权限不足
     */
    AUTH_LIMIT(10, "权限不足"),
    /**
     * 参数错误
     */
    ILLEGAL_ARGUMENT(2, "参数错误"),
    /**
     * 服务器繁忙
     */
    SERVER_BUSY(3, "服务器繁忙"),
    /**
     * 请求太频繁
     */
    REQUEST_LIMIT(4, "请求太频繁"),
    /**
     * 恶意请求
     */
    BLACK_LIMIT(5, "恶意请求"),
    /**
     * 需要跳转统一权限登录
     */
    OAUTH_NEED_LOGIN(6, "需要登录");

    private final int code;
    private final String desc;

    ResponseCode(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

}