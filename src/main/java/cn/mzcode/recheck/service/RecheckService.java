package cn.mzcode.recheck.service;

import cn.mzcode.recheck.entity.RecheckSim;
import cn.mzcode.recheck.resp.ServerResponse;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * @author: 马壮
 * @create: 2020-04-28 22:14
 * @description: 查重服务接口
 */
public interface RecheckService {

    /**
     * 保存上传文件，创建查重编号
     *
     * @param file 上传的文件
     * @return 查重编码
     */
    String saveSrcFile(MultipartFile file) throws IOException;

    /**
     * 根据查重编号，解压文件
     *
     * @param recheckNo 查重编号
     * @return
     */
    boolean unzipByRecheckNo(String recheckNo) throws Exception;

    /**
     * 预校验文件夹结构、文件名是否符合要求
     * 如不符合，直接删除
     *
     * @param recheckNo 查重编号
     * @return
     */
    ServerResponse preVerifyStructure(String recheckNo);

    /**
     * 根据查重编号清除文件
     *
     * @param recheckNo 查重编号
     */
    void cleanFileByRecheckNo(String recheckNo);

    /**
     * 开始异步计算相似度
     *
     * @param recheckNo 查重编号
     */
    void startAsyncCalcSim(String recheckNo);

    /**
     * 保存查重结果
     *
     * @param result    查重结果对象
     * @param recheckNo 查重编号
     * @return
     */
    void saveRecheckResult(List<RecheckSim> result, String recheckNo);

    /**
     * 加载查重结果
     *
     * @param recheckNo 查重编号
     * @return
     */
    List<RecheckSim> loadRecheckResult(String recheckNo);

    /**
     * 查看是否查重完成，若完成，在upload/result文件夹会有查重编号同名的对象文件
     *
     * @param recheckNo 查重编号
     * @return 是否查重完成
     */
    boolean recheckComplete(String recheckNo);

    /**
     * 查看是否存在指定查重编号的上传文件
     *
     * @param recheckNo 查重编号
     * @return 查重编号是否有效
     */
    boolean recheckUploaded(String recheckNo);

    /**
     * 获取查重结果目录
     *
     * @return
     */
    String getResultPath();
}
