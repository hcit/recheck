package cn.mzcode.recheck;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.system.ApplicationHome;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileNotFoundException;

@EnableAsync
@SpringBootApplication
public class RecheckApplication {

    public static void main(String[] args) {
        /**
         * 创建文件夹
         * src:源压缩包
         * unzip:解压目录
         * result:查重结果目录
         */
        String rootPath = new ApplicationHome(RecheckApplication.class).getSource().getParentFile().getPath() + "/upload";
        File savePath = new File(rootPath + "/src");
        if (!savePath.exists()) {
            savePath.mkdirs();
        }
        savePath = new File(rootPath + "/unzip");
        if (!savePath.exists()) {
            savePath.mkdirs();
        }
        savePath = new File(rootPath + "/result");
        if (!savePath.exists()) {
            savePath.mkdirs();
        }
        SpringApplication.run(RecheckApplication.class, args);
    }

}
