package cn.mzcode.recheck.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author: 马壮
 * @create: 2020-04-29 22:34
 * @description: 相似度结果
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RecheckSim implements Serializable {

    /**
     * 用户1
     */
    private String user1;

    /**
     * 用户2
     */
    private String user2;

    /**
     * 相似度
     */
    private double sim;
}
