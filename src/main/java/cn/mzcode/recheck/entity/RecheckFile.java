package cn.mzcode.recheck.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

/**
 * @author: 马壮
 * @create: 2020-04-29 21:09
 * @description: 查重文件实体
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RecheckFile {

    /**
     * 用户名（文件名）
     */
    private String userName;

    /**
     * 词频
     */
    private Map<String, Integer> wordFrequency;

    /**
     * 去除指定词频
     *
     * @param exception 待去除的词频
     */
    public RecheckFile excludeWordFrequency(Map<String, Integer> exception) {
        exception.entrySet().stream().forEach(entry -> {
            if (this.wordFrequency.containsKey(entry.getKey())) {
                Integer count = this.wordFrequency.get(entry.getKey());
                count = count - entry.getValue();
                this.wordFrequency.put(entry.getKey(), count < 0 ? 0 : count);
            }
        });
        return this;
    }
}
